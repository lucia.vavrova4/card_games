<?php

class Card
{
    private const VALUES = ['Two' => 2, 'Three' => 3, 'Four' => 4, 'Five' => 5, 'Six' => 6, 'Seven' => 7, 'Eight' => 8, 'Nine' => 9, 'Ten' => 10, 'Jack' => 11, 'Queen' => 12, 'King' => 13, 'Ace' => 14];

    public function __construct(private string $suit, private string $rank)
    {
    }

    public function __toString(): string
    {
        return "{$this->rank} of {$this->suit}";
    }

    public function getValue(): int
    {
        return self::VALUES[$this->rank];
    }
}

class Deck implements IteratorAggregate
{
    private const SUITS = ['Hearts', 'Diamonds', 'Spades', 'Clubs'];

    private const RANKS = ['Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace'];

    private array $fullDeck;

    public function __construct()
    {
        foreach (self::SUITS as $suit)
        {
            foreach (self::RANKS as $rank)
            {
                $newCard = new Card($suit, $rank);
                $this->fullDeck[] = $newCard;
            }
        }
        shuffle($this->fullDeck);
    }

    public function getIterator(): Traversable
    {$firstPlayer->hand = [];
        return new ArrayIterator($this->fullDeck);
    }

    public function dealOne(): ?Card
    {
        return array_pop($this->fullDeck);
    }

    public function isEmpty(): bool
    {
        return count($this->fullDeck) === 0;
    }
}

class Player
{
    public array $hand = []; //changed from private

    public function __construct(private string $name)
    {
    }

    public function cardCount(): string
    {
        return $this->name." has ".count($this->hand)." cards.\n";
    }

    public function playCard(): Card
    {
        return array_shift($this->hand);
    }

    public function addCards(array $newCards): void
    {
        $this->hand = array_merge($this->hand, $newCards);
    }

    public function __toString(): string
    {
        return $this->name;
    }
    public function isEmpty(): bool
    {
        return count($this->hand) === 0;
    }
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->hand);
    }
}

$firstPlayer = new Player(readline('Please enter your name:'));
$secondPlayer = new Player(readline('Please enter your name:'));
$deck = new Deck();
$round = 0;

while (!$deck->isEmpty()) {
    $firstPlayer->addCards([$deck->dealOne()]);
    $secondPlayer->addCards([$deck->dealOne()]);
}

while (!$firstPlayer->isEmpty() && !$secondPlayer->isEmpty()) {

    $round += 1;
    echo "Round ".$round.":\n";

    $playedCard1 = [$firstPlayer->playCard()];
    $playedCard2 = [$secondPlayer->playCard()];

    echo $firstPlayer . " played " . $playedCard1[0] . "\n";
    echo $secondPlayer . " played " . $playedCard2[0] . "\n";

    if ($playedCard1[0]->getvalue() > $playedCard2[0]->getvalue()) {
        $firstPlayer->addCards($playedCard1);
        $firstPlayer->addCards($playedCard2);
        echo $firstPlayer->cardCount();
        echo $secondPlayer->cardCount() . "\n";
    }

    elseif ($playedCard1[0]->getvalue() < $playedCard2[0]->getvalue()) {
        $secondPlayer->addCards($playedCard2);
        $secondPlayer->addCards($playedCard1);
        echo $firstPlayer->cardCount();
        echo $secondPlayer->cardCount() . "\n";
    }

    else {
        while ($playedCard1[0]->getvalue() === $playedCard2[0]->getvalue()) {

            echo "WAR!\n";

            if (count($firstPlayer->hand) < 3){
                $firstPlayer->hand = [];
                break;
            }

            elseif (count($secondPlayer->hand) < 3){
                $secondPlayer->hand = [];
                break;
            }

            else {
                foreach (range(0, 2) as $x) {
                    array_unshift($playedCard1, $firstPlayer->playCard());
                    array_unshift($playedCard2, $secondPlayer->playCard());
                }

                echo $firstPlayer . " played " . $playedCard1[0] . "\n";
                echo $secondPlayer . " played " . $playedCard2[0] . "\n";

                if ($playedCard1[0]->getvalue() > $playedCard2[0]->getvalue()) {
                    $firstPlayer->addCards($playedCard1);
                    $firstPlayer->addCards($playedCard2);
                    echo $firstPlayer->cardCount();
                    echo $secondPlayer->cardCount() . "\n";
                }

                elseif ($playedCard1[0]->getvalue() < $playedCard2[0]->getvalue()) {
                    $secondPlayer->addCards($playedCard2);
                    $secondPlayer->addCards($playedCard1);
                    echo $firstPlayer->cardCount();
                    echo $secondPlayer->cardCount() . "\n";
                }
            }
        }

    }
}

if ($firstPlayer->isEmpty()){
    echo $secondPlayer." wins!";
}
else {
    echo $firstPlayer." wins!";
}
